package org.paumard.collectors.util;

import org.paumard.collectors.model.Actor;
import org.paumard.collectors.model.Movie;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MovieReader {


    public Set<Movie> readMovies() throws IOException {
        Stream<String> lines =
                Files.lines(Paths.get("files/movies-mpaa.txt"), StandardCharsets.ISO_8859_1);

        Set<Movie> movies = lines.map(
                (String line) -> {
                    String[] elements = line.split("/");
                    String title = extractTitle(elements[0]);
                    String releaseYear = extractReleaseYear(elements[0]);

                    Movie movie = new Movie(title, Integer.valueOf(releaseYear));

                    Stream.of(elements).skip(1).map(this::extractActor).forEach(movie::addActor);

                    return movie;
                }
        ).collect(Collectors.toSet());

        System.out.println("# movies : " + movies.size());

        return movies;
    }

    private Actor extractActor(String elements) {
        String[] nameElements = elements.split(", ");
        String lastName = extractLastName(nameElements);
        String firstName = extractFirstName(nameElements);

        return new Actor(lastName, firstName);
    }

    private String extractFirstName(String[] nameElements) {
        String firstName = "";
        if (nameElements.length > 1) {
            firstName = nameElements[1].trim();
        }
        return firstName;
    }

    private String extractLastName(String[] name) {
        return name[0].trim();
    }

    private String extractReleaseYear(String element) {
        String releaseYear = element.substring(element.lastIndexOf("(") + 1, element.lastIndexOf(")"));
        if (releaseYear.contains(","))
            releaseYear = releaseYear.substring(0, releaseYear.indexOf(","));
        return releaseYear;
    }

    private String extractTitle(String element) {
        return element.substring(0, element.lastIndexOf("(")).trim();
    }

}
