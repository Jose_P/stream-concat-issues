package org.paumard.collectors;

import org.junit.Test;
import org.paumard.collectors.model.Actor;
import org.paumard.collectors.model.Movie;
import org.paumard.collectors.util.MovieReader;

import java.io.IOException;
import java.util.Collection;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.function.Function.identity;

public class CollectorsTest {

    public static <T, U> Collector<T, ?, Stream<U>> flattenToStreamV1(Function<T, ? extends Collection<U>> mapper) {
        Objects.requireNonNull(mapper);

        // This implementation can throw a StackOverflowExceptions for large streams
        return Collectors.reducing(
                Stream.empty(),
                t -> mapper.apply(t).stream(),
                (stream1, stream2) -> Stream.of(stream1, stream2).flatMap(identity()));
    }

    public static <T, U> Collector<T, ?, Stream<U>> flattenToStreamV2(Function<T, ? extends Collection<U>> mapper) {
        Objects.requireNonNull(mapper);

        return Collector.<T, Stream.Builder<U>, Stream<U>>of(
                Stream::builder,
                (builder, element) -> mapper.apply(element).forEach(builder),
                (builder1, builder2) -> {
                    builder2.build().forEach(builder1);
                    return builder1;
                },
                Stream.Builder::build
        );
    }

    @Test
    public void should_create_the_set_of_all_distinct_actors_with_classical_flatmap() throws IOException {
        MovieReader movieReader = new MovieReader();

        Set<Movie> movies = movieReader.readMovies();
        Set<Actor> actors = movies.stream().flatMap(movie -> movie.actors().stream()).distinct().collect(Collectors.toSet());

        System.out.println("movies.size() = " + movies.size());
        System.out.println("actors.size() = " + actors.size());
    }

    @Test
    public void should_create_the_set_of_all_distinct_actors_with_flatmap_collector_using_Stream_of_then_flatmap() throws IOException {

        MovieReader movieReader = new MovieReader();
        Set<Movie> movies = movieReader.readMovies();

        Function<Movie, Set<Actor>> mapper = Movie::actors;
        Collector<Movie, ?, Stream<Actor>> movieStreamCollectorV1 = flattenToStreamV1(mapper);

        Set<Actor> actorsV1 = movies.stream() // .limit(1000)
                .collect(movieStreamCollectorV1).collect(Collectors.toSet());

        System.out.println("V1 actors.size() = " + actorsV1.size());
    }

    @Test
    public void should_create_the_set_of_all_distinct_actors_with_map_then_reduce() throws IOException {

        MovieReader movieReader = new MovieReader();
        Set<Movie> movies = movieReader.readMovies();

        Set<Actor> actors = movies.stream() // .limit(1000)
                .map(movie -> movie.actors().stream())
                .reduce(
                        Stream.empty(),
                        (s1, s2) -> Stream.of(s1, s2).flatMap(Function.identity()))
                .collect(Collectors.toSet());

        System.out.println("V11 actors.size() = " + actors.size());
    }

    @Test
    public void should_create_the_set_of_all_distinct_actors_with_flatmap_collector_using_StreamBuilder() throws IOException {

        MovieReader movieReader = new MovieReader();

        Set<Movie> movies = movieReader.readMovies();

        Function<Movie, Set<Actor>> mapper = Movie::actors;
        Collector<Movie, ?, Stream<Actor>> movieStreamCollectorV2 = flattenToStreamV2(mapper);

        Set<Actor> actorsV2 = movies.stream().collect(movieStreamCollectorV2).collect(Collectors.toSet());

        System.out.println("V2 actors.size() = " + actorsV2.size());
    }

    @Test
    public void should_create_the_set_of_all_distinct_actors_with_reduce_in_a_builder_and_collect() throws IOException {

        MovieReader movieReader = new MovieReader();

        Set<Movie> movies = movieReader.readMovies();

        Set<Actor> actors =
                movies.stream()
                        .map(movie -> movie.actors().stream())
                        .reduce(
                                Stream.<Actor>builder(),
                                (builder, stream) -> {
                                    stream.forEach(builder::add);
                                    return builder;
                                },
                                (builder1, builder2) -> {
                                    builder2.build().forEach(builder1::add);
                                    return builder1;
                                })
                        .build().collect(Collectors.toSet());

        System.out.println("V21 actors.size() = " + actors.size());
    }
}
